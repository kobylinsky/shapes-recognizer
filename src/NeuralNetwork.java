import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NeuralNetwork {

    public static List<Neuron> neurons;
    private static Neuron predicted;

    private static int[][] input;

    public static Neuron getPrediction(BufferedImage image) {
        image = resizeTo200(image);
        /*try {
            ImageIO.write(image, "png", new File("test.png"));
        } catch (IOException ioe) {ioe.printStackTrace();}    */

        input = new int[image.getHeight()][];
        for (int y = 0; y < image.getHeight(); y++) {
            input[y] = new int[image.getWidth()];
            for (int x = 0; x < image.getWidth(); x++)
                if (image.getRGB(x, y) == 0xFF000000)
                    input[y][x] = 1;
        }

        List<String> shapes = Shapes.getAll();
        List<Float> weights = new ArrayList<Float>();
        neurons = new ArrayList<Neuron>();

        float sums = 0;
        for (String shape : shapes) {
            Neuron shapeNeuron = new Neuron(input, shape);
            neurons.add(shapeNeuron);
            weights.add(shapeNeuron.getSum());
            sums += shapeNeuron.getSum();
        }
        System.out.println();
        for (Neuron n : neurons)
            System.out.println(n.getName() + " - " + 100f * n.getSum() / sums + "%");

        float maxWeight = weights.get(0);
        int maxShapeIndex = 0;
        for (int i = 0; i < weights.size(); i++)
            if (maxWeight < weights.get(i)) {
                maxWeight = weights.get(i);
                maxShapeIndex = i;
            }

        predicted = neurons.get(maxShapeIndex);
        return predicted;
    }

    private static BufferedImage resizeTo200(BufferedImage image) {
        int offsetTop = 0, offsetBottom = 0, offsetLeft = 0, offsetRight = 0;
        boolean add;
        for (int i = 0; i < 200; i++) {
            add = true;
            for (int j = 0; j < 200; j++)
                if (image.getRGB(j, i) == 0xFF000000) {
                    add = false;
                    break;
                }
            if (add) offsetTop++;
            else break;
        }
        //System.out.println("Top:" + offsetTop);

        for (int i = 0; i < 200; i++) {
            add = true;
            for (int j = 0; j < 200; j++)
                if (image.getRGB(i, j) == 0xFF000000) {
                    add = false;
                    break;
                }
            if (add) offsetLeft++;
            else break;
        }
        //System.out.println("Left:" + offsetLeft);

        for (int i = 199; i >= 0; i--) {
            add = true;
            for (int j = 0; j < 200; j++)
                if (image.getRGB(j, i) == 0xFF000000) {
                    add = false;
                    break;
                }
            if (add) offsetBottom++;
            else break;
        }
        //System.out.println("Bootom:" + offsetBottom);

        for (int i = 199; i >= 0; i--) {
            add = true;
            for (int j = 0; j < 200; j++)
                if (image.getRGB(i, j) == 0xFF000000) {
                    add = false;
                    break;
                }
            if (add) offsetRight++;
            else break;
        }
        //System.out.println("Right:" + offsetRight);

        BufferedImage newImage = new BufferedImage(200 - offsetLeft - offsetRight, 200 - offsetTop - offsetBottom, BufferedImage.TYPE_INT_ARGB);
        for (int i = 0; i < newImage.getWidth(); i++)
            for (int j = 0; j < newImage.getHeight(); j++)
                newImage.setRGB(i, j, image.getRGB(i + offsetLeft, j + offsetTop));
        return scaleImage(newImage, 200, 200, Color.white);
    }

    private static BufferedImage scaleImage(BufferedImage img, int width, int height, Color background) {
        int imgWidth = img.getWidth();
        int imgHeight = img.getHeight();
        if (imgWidth * height < imgHeight * width)
            width = imgWidth * height / imgHeight;
        else
            height = imgHeight * width / imgWidth;
        BufferedImage newImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = newImage.createGraphics();
        try {
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g.setBackground(background);
            g.clearRect(0, 0, 200, 200);
            g.drawImage(img, (200 - width) / 2, (200 - height) / 2, width, height, null);
        } finally {
            g.dispose();
        }
        return newImage;
    }

    public static int[][] getInput() {
        return input;
    }

    public static Neuron getPredicted() {
        return predicted;
    }

    public static Neuron getNeuronByName(String s) {
        for (Neuron n : neurons)
            if (n.getName().equals(s))
                return n;
        return null;
    }

    public static void refreshWeightsForCorrect(Neuron correctNeuron, Integer runs) {
        for (Neuron neuron : neurons) {
            float[][] weights = neuron.getWeights();
            for (int i = 0; i < 200; i++)
                for (int j = 0; j < 200; j++)
                    if (NeuralNetwork.getInput()[i][j] == 1)
                        weights[i][j] += (neuron.equals(correctNeuron) ? 0.01 * runs : -0.01 * runs);
            try {
                Shapes.writeWeightsToFile(neuron.getName(), weights);
            } catch (IOException ignored) {
            }
        }
    }
}