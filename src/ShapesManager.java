import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;

public class ShapesManager  extends JFrame {

    private JPanel content;
    private JTabbedPane tabs;
    private JButton addShape;
    private JButton buttonSaveThisNetwork;
    private JButton buttonLoadNetwork;

    public ShapesManager(){
        refreshTabs();
        setContentPane(content);
        setLocation(250, 250);
        pack();
        setTitle("Neural network memory");
        addShape.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Shapes.addShape(JOptionPane.showInputDialog(new JFrame(), "New shape name:"));
                } catch (IOException ignored) {}
                refreshTabs();
            }
        });
        buttonSaveThisNetwork.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String networkName = JOptionPane.showInputDialog(new JFrame(), "Set the name of network:", GregorianCalendar.getInstance().getTime().toString().replace(" ","_").replace(":","."));
                if(networkName != null && networkName.length() > 0){
                    try {
                        createNewNetwork(networkName);
                    } catch (IOException ignored) {}
                }
            }
        });
        List<String> networks = new ArrayList<String>();
        File dir = new File("weights/networks/");
        if (!dir.exists()){
            dir.mkdir();
        }
        for (File file : dir.listFiles()) {
            String fileName = file.getName();
            if (fileName.toLowerCase().endsWith((".nnweis")))
                networks.add(fileName.substring(0, fileName.length() - 7));
        }
        if(networks.size()==0)
            buttonLoadNetwork.setVisible(false);
        buttonLoadNetwork.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> networks = new ArrayList<String>();
                File dir = new File("weights/networks/");
                for (File file : dir.listFiles()) {
                    String fileName = file.getName();
                    if (fileName.toLowerCase().endsWith((".nnweis")))
                        networks.add(fileName.substring(0, fileName.length() - 7));
                }
                String selectedNetwork = (String) JOptionPane.showInputDialog(ShapesManager.this,
                    "Select the network to load", "Neural networks", JOptionPane.INFORMATION_MESSAGE,
                    null, networks.toArray(new String[networks.size()]), networks.get(0));
                try {
                    loadNewNetwork(selectedNetwork);
                } catch (IOException ignored) {}
            }
        });
    }

    private void loadNewNetwork(String selectedNetwork) throws IOException {
        DataInputStream in = new DataInputStream(new FileInputStream("weights/networks/" + selectedNetwork + ".nnweis"));
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String fileString = null;
        try {
            fileString = br.readLine();
        } catch (IOException ignored) {}
        String[] shapesArray = fileString != null ? fileString.split("---.shapes_separator.---") : new String[0];
        Map<String,String> newShapes = new HashMap<String, String>();
        for(String shapeString:shapesArray){
            String[] shapeArray = shapeString.split("-_-shape_name-_-");
            newShapes.put(shapeArray[0],shapeArray[1]);
        }
        List<String> oldShapes = Shapes.getAll();
        for(String shape:oldShapes)
            (new File("weights/" + shape + ".wei")).delete();
        for(String newShapeName:newShapes.keySet()){
            File yourFile = new File("weights/" + newShapeName + ".wei");
            if(!yourFile.exists()) {
                try {
                    yourFile.createNewFile();
                } catch (IOException e) {e.printStackTrace();}
            }
            FileOutputStream fos = new FileOutputStream(yourFile, false);
            fos.write(newShapes.get(newShapeName).getBytes());
            fos.flush();
            fos.close();
        }

        in.close();
    }

    private void createNewNetwork(String networkName) throws IOException {
        File yourFile = new File("weights/networks/" + networkName + ".nnweis");
        if(!yourFile.exists()) {
            try {
                yourFile.createNewFile();
            } catch (IOException e) {e.printStackTrace();}
        }
        FileOutputStream fos = new FileOutputStream(yourFile, false);

        List<String> shapes = Shapes.getAll();

        for(String shape : shapes){
            float[][] weights = Shapes.getShapeWeights(shape);
            fos.write(shape.getBytes());
            fos.write("-_-shape_name-_-".getBytes());
            for (float[] weight : weights) {
                for (float aWeight : weight)
                    fos.write((String.valueOf(aWeight) + ",").getBytes());
                fos.write(";".getBytes());
            }
            fos.write("---.shapes_separator.---".getBytes());
        }
        fos.flush();
        fos.close();
    }

    private void refreshTabs(){
        while (tabs.getTabCount() > 0)
            tabs.remove(0);
        List<String> shapes = Shapes.getAll();
        for(final String s:shapes){
            JPanel tabContent = new JPanel();
            JPanel weightsContent = new JPanel();
            weightsContent.setPreferredSize(new Dimension(200, 200));
            weightsContent.setBackground(Color.WHITE);

            BufferedImage image = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = image.createGraphics();
            weightsContent.paint(g2d);
            float[][] weights = Shapes.getShapeWeights(s);
            for(int i = 0; i < 200; i++){
                for(int j = 0; j < 200; j++){
                    int grayscale = (int)(255*(1 - weights[i][j]));
                    image.setRGB(j, i,(new Color(grayscale, grayscale, grayscale)).getRGB());
                }
            }
            JLabel picLabel = new JLabel(new ImageIcon(image));
            weightsContent.add(picLabel);
            JButton removeButton = new JButton("Delete");
            removeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Shapes.removeShape(s);
                    refreshTabs();
                }
            });
            tabContent.add(weightsContent);
            tabContent.add(removeButton);
            tabs.add(s, tabContent);
        }
    }
}
