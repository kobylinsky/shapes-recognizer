import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class DrawingPanel extends JPanel implements ActionListener, MouseListener, MouseMotionListener {

    private List<ArrayList<Point>> points = new ArrayList<ArrayList<Point>>();
    private boolean clearAll = false;

    public DrawingPanel() {
        super();
        addMouseMotionListener(this);
        addMouseListener(this);
        setDoubleBuffered(true);
        setBackground(Color.white);
        //System.out.println("DrawingPanel created!");
    }

    @Override
    public void paintComponent(Graphics g) {
        //super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.white);
        g2.fillRect(0, 0, 200, 200);
        for (ArrayList<Point> lines : points) {
            g2.setColor(Color.black);
            g2.setStroke(new BasicStroke(4));
            for (int i = 0; i < lines.size() - 1; i++)
                g2.drawLine(lines.get(i).x, lines.get(i).y, lines.get(i + 1).x, lines.get(i + 1).y);
        }
        if (clearAll) {
            g2.setColor(Color.white);
            g2.fillRect(0, 0, 200, 200);
            clearAll = false;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //System.out.println("---Pressed");
        ArrayList<Point> cPoints = new ArrayList<Point>();
        cPoints.add(new Point(e.getX(), e.getY()));
        points.add(cPoints);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // System.out.println("---Dragged");
        ArrayList<Point> cPoints = points.get(points.size() - 1);
        cPoints.add(new Point(e.getX(), e.getY()));
        points.add(cPoints);
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //System.out.println("---Released");
        ArrayList<Point> cPoints = points.get(points.size() - 1);
        cPoints.add(new Point(e.getX(), e.getY()));
        points.add(cPoints);
        repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    public void clear() {
        clearAll = true;
        points.clear();
        repaint();
    }
}